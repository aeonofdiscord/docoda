const fs = require('fs');
const remote = require('electron').remote;
const app = remote.app;

const sections = ['view', 'edit', 'index'];

let page = {
	title:'',
	content:'',
	html:'',
	exists: false,
};

let index = {};

let state = {
	bold: false,
	italic: false,
};

let history = {
	pages: [],
	pos: 0,
};

function addNode(p, e) {
	let n = document.createElement(e);
	p.appendChild(n);
	return n;
}

function addText(n, t) {
	if(n.lastChild && n.lastChild instanceof Text) {
		n.lastChild.data += t;
	} else {
		n.appendChild(document.createTextNode(t));
	}
}

function appFolder() {
	return app.getPath("home")+"/.docoda";
}

function bold(state, node) {
	let text = document.createElement('span');
	state.bold = true;

	while(state.code.length) {
		let t = next(state);
		if(t == '*') {
			token(state);
			let b = document.createElement('b');
			b.appendChild(text);
			node.appendChild(b);
			state.bold = false;
			return;
		} else if(t == '\n' || (t == '/' && state.italic)) {
			break;
		} else if(isSymbol(t)) {
			token(state);
			element(state, t, text);
		} else {
			token(state);
			addText(text, t);
		}
	}
	prependText(text, '*');
	node.appendChild(text);	
	state.bold = false;
}

function clear(n) {
	while(n.firstChild)
		n.removeChild(n.firstChild);
}

function consume(n) {
	state.code = state.code.slice(n);
}

function deletePage() {
	if(page.exists && confirm("Are you sure you want to delete this page?")) {
		let path = getPath(page.title);
		fs.unlink(path, logError);
		delete index[smash(page.title)];
		load("root");
	}
}

function editMode() {
	showSection("edit");
	getID("edit-title").value = page.title;
	getID("edit-content").value = page.content;
}

function element(state, t, node) {
	switch(t) {
		case '*': bold(state, node); break;
		case '/': italic(state, node); break;
		case '[': link(state, node); break;
		case '\\': literal(state, node); break;
		case '___': addNode(node, 'hr'); break;
		default: text(state, t, node); break;
	}
}

function format(code) {
	let div = document.createElement("div");
	state.code = code;
	while(state.code.length) {
		line(state, div);
	}
	return div;
}

function getID(id) { return document.getElementById(id); }

function getLinksIn(title, f) {
	let page = index[smash(title)];
	if(!page.content) return [];
	let links = [];
	let re = /(?:(?!\\))\[(.*?)(?:\|(.*?))?\]/g;
	let res = null;
	do {
		res = re.exec(page.content);
		if(!res)
			return links;
		links.push(res[2] || res[1]);
	} while(res);
	console.log(links);
	return links;
}

function getPath(title) {
	return pageFolder()+'/'+smash(title)+".json";
}

function isSymbol(c) {
	switch(c) {
		case '*':
		case '-':
		case '/':
		case '[':
		case ']':
		case '\n':
			return true;
		default: return false;
	}
}

function italic(state, node) {
	let text = document.createElement('span');
	state.italic = true;

	while(state.code.length) {
		let t = next(state);
		if(t == '/') {
			token(state);
			let i = document.createElement('i');
			i.appendChild(text);
			node.appendChild(i);
			state.italic = false;
			return;
		} else if(t == '\n' || (t == '*' && state.bold)) {
			break;
		} else if(isSymbol(t)) {
			token(state);
			element(state, t, text);
		} else {
			token(state);
			addText(text, t);
		}
	}
	prependText(text, '/');
	node.appendChild(text);	
	state.italic = false;
}

function line(state, node) {
	let t = token(state);
	if(t == '-') {
		list(state, node);
	} else {
		while(state.code.length && t != '\n') {
			element(state, t, node);
			t = token(state);
		}
		addText(node, '\n');
	}
}

function link(state, node) {
	let s = '';
	let redirect = undefined;
	let mode = "link";
	
	while(state.code.length > 0) {
		let c = state.code[0];
		let t = next(state);
		
		if(t == ']') {
			token(state);
			break;
		} else if(t == '\n') {
			break;
		} else if(t == '|') {
			token(state);
			mode = 'redirect';
			redirect = '';
		} else {
			token(state);
			if(mode == "redirect") {
				redirect += t;
			} else {
				s += t;
			}
		}
	}
	if(mode == "redirect")
		node.append(tolink(redirect, s));
	else
		node.append(tolink(s));
}

function list(state, node) {
	let ul = addNode(node, "ul");
	listItem(state, ul);
	while(state.code.length && state.code[0] == '-') {
		consume(1);
		listItem(state, ul);
	}
}

function listItem(state, node) {
	let li = addNode(node, 'li');
	let t = token(state);
	if(t == '\n') {
		consume(1);
		return;
	}
	element(state, t, li);
	while(state.code.length) {
		t = next(state);
		if(t == '\n') {
			consume(1);
			return;
		} else {
			consume(t.length);
			element(state, t, li);
		}
	}
}

function literal(state, node) {
	if(state.code.length > 0) {
		let c = state.code[0];
		consume(1);
		node.appendChild(document.createTextNode(c));
	}
}

function load(title, keepHistory) {
	page = index[smash(title)];
	if(page) {
		page.html = format(page.content);
		page.exists = true;
	}
	else {
		page = {title:title, content:'', html:document.createTextNode(''), exists:false};
	}
	viewMode();
	if(!keepHistory) {
		history.pages = history.pages.slice(0, history.pos);
		++history.pos;
		history.pages.push(title);
	}
}

async function loadPages() {
	return new Promise((rs, rj) => {
		fs.readdir(pageFolder(), (e, items)=>{
			if(e) {
				rj(e);
			} else {
				for(let f of items) {
					let data = fs.readFileSync(pageFolder()+'/'+f);
					let d = JSON.parse(data);
					index[smash(d.title)] = d;
				}
				rs(true);
			}
		});
	});
}

function logError(e) {
	if(e) { console.log(e); }
}

function next(state) {
	if(!state.code.length) {
		return null;
	}
	let i = 0;
	let c = state.code[i];
	if(isSymbol(c)) return c;
	let s = '';
	while(i < state.code.length) {
		c = state.code[i];
		if(isSymbol(c)) {
			return s;
		} else if(c == '_') {
			if(state.code.substring(i, 3) == '__\n') {
				s = "___";
				return s;
			} else {
				++i;
				s += '_';
			}
		} else {
			++i;
			s += c;
		}
	}
	return s;
}

function pageExists(title) {
	return fs.existsSync(getPath(title));
}

function pageFolder() {
	return appFolder()+"/pages";
}

function prependText(n, text) {
	if(!n.firstChild) {
		addText(n, text);
	} else if(n.firstChild instanceof Text) {
		n.firstChild.data = text+n.firstChild.data;
	} else {
		n.insertBefore(document.createTextNode(text), n.firstChild);
	}
}

function readFile(path, f) {
	fs.readFile(path, {'encoding':'utf8'}, f);
}

function save(title, content) {
	let data = JSON.stringify({
		"title": title,
		"content": content,
	});
	fs.writeFile(getPath(title), data, {'encoding':'utf8'}, logError);
	if(!index[smash(title)])
		index[smash(title)] = title;
	page.html = format(content);
	page.exists = true;
	viewMode();
}

function savePage() {
	page = {
		title : getID("edit-title").value,
		content : getID("edit-content").value,
	};
	save(page.title, page.content);
	index[smash(page.title)] = page;
}

function setText(n, t) {
	clear(n);
	addText(n, t);
}

function showBacklinks() {
	let backlinks = [];
	for(let p of Object.values(index)) {
		let links = getLinksIn(p.title);

		if(links.includes(page.title)) {
			backlinks.push(p.title);
		}
	}

	showSection("index");
	setText(getID("index-header"), "Backlinks");
	clear(getID("index-content"));
	let ul = document.createElement("ul");
	getID("index-content").appendChild(ul);
	for(let l of backlinks) {
		let li = addNode(ul, 'li');
		li.appendChild(tolink(l));
	}
}

function showIndex() {
	let pages = Object.keys(index);
	pages.sort();
	showSection('index');
	clear(getID("index-content"));
	getID("index-header").innerHTML = "Index";
	let ul = document.createElement("ul");

	for(let p of pages) {
		let li = document.createElement('li');
		li.appendChild(tolink(index[p].title));
		ul.appendChild(li);
	}
	getID("index-content").appendChild(ul);
	getID("index-content").appendChild(document.createElement('p')).appendChild(document.createTextNode(pages.length+" pages."));
}

function showRedlinks() {
	showSection('index');
	clear(getID("index-content"));
	let ul = document.createElement("ul");
	getID("index-header").innerHTML = "Redlinks";
	getID("index-content").appendChild(ul);

	let pages = ["root"];
	let done = {};

	while(pages.length) {
		let p = pages.splice(0, 1)[0];
		let links = getLinksIn(p);

		for(var l of links) {
			if(done[smash(l)]) continue;
			if(!index[smash(l)]) {
				let li = document.createElement('li');
				li.appendChild(tolink(l));
				ul.appendChild(li);
			} else {
				pages.push(l);
			}
			done[smash(l)] = true;
		}
	}
}

function showSection(id) {
	for(let s of sections) {
		getID(s).style = "display:none";
	}
	getID(id).style = '';
}

function smash(s) {
	return s.replace(/[^a-z0-9]/gi, '_').toLowerCase();
}

function text(state, t, node) {
	addText(node, t);
}

function tolink(title, text) {
	text = text || title;
	let a = document.createElement('a');
	a.href="";
	addText(a, text);
	a.onclick = (e)=>{
		e.preventDefault();
		load(title);
		return false;
	};
	if(!index[smash(title)]) {
		a.classList.add("new-page-link");
	}
	return a;
}

function token(state) {
	if(!state.code.length) {
		return null;
	}

	let c = state.code[0];

	if(isSymbol(c)) { 
		consume(1);
		return c; 
	}

	let s = '';
	while(state.code.length) {
		c = state.code[0];
		if(isSymbol(c)) {
			return s;
		} else if(c == '_') {
			if(state.code.substring(0, 3) == '__\n') {
				consume(3);
				s = '___';
				return s;
			} else {
				consume(1);
				s += '_';
			}
		} else {
			consume(1);
			s += c;
		}
	}
	return s;
}

function viewMode() {
	showSection('view');
	getID("title").innerHTML = page.title || 'page';
	clear(getID("content"));
	getID("content").appendChild(page.html);
}

async function init() {
	let link = document.createElement("link");
	link.rel = "stylesheet";
	link.href = appFolder()+"/custom.css";
	document.head.appendChild(link);
	for(let f of [appFolder(), pageFolder()]) {
		try {
			fs.mkdirSync(f);
		} catch(e) {
			if(e.code != "EEXIST")
				alert("Unable to create data directory. Pages will not be saved!");
		}
	}

	await loadPages();
	getID("nav-back").onclick = ()=>{
		if(history.pos > 1) {
			--history.pos;
			load(history.pages[history.pos-1], true);
		}
	};
	getID("nav-forward").onclick = ()=>{
		if(history.pos <= history.pages.length-1) {
			++history.pos;
			load(history.pages[history.pos-1], true);
		}
	};
	getID("a-thispage").onclick = ()=>{
		viewMode();
	};
	getID("a-redlinks").onclick = ()=>{
		showRedlinks();
	};
	getID("a-backlinks").onclick = ()=>{
		showBacklinks();
	};
	getID("a-index").onclick = ()=>{
		showIndex();
	};
	getID("a-root").onclick = ()=>{
		load("root");
	};
	load("root");
}
