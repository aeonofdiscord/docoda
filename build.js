const https = require('https');
const http = require('http');
const asar = require('asar');
const os = require('os');
const fs = require('fs');
const extract = require('extract-zip');
const {URL} = require('url');
const {resolve} = require('path');
const plist = require('plist');

const platform = os.platform();
const arch = os.arch();
const electron_version = 'v2.0.8';

const builddir = "build/"+platform+'-'+arch+'/'

const url = 'https://github.com/electron/electron/releases/download/'+electron_version+'/electron-'+electron_version+'-'+platform+'-'+arch+'.zip';

function mkdir(p) {
	if (!fs.existsSync(p)){
		fs.mkdirSync(p);
	}
}

for(let d of ["build", "package", builddir]) {
	mkdir(d);
}

let types = [
	'js',
	'css',
	'json',
	'html',
	'txt',
	'md',
];

let exclude = [
	'package-lock.json',
];

function isdir(path) {
	return fs.statSync(path).isDirectory();
}

for(let f of fs.readdirSync(".", {encoding:"utf8"})) {
	if(exclude.includes(f)) continue;
	for(let t of types) {
		if(f.endsWith('.'+t)) {
			fs.copyFileSync(f, 'package/'+f);
			break;
		}
	}
}

const zip = "build/electron-"+platform+'-'+arch+".zip";
let resdir = builddir+"resources/";
if(platform == "darwin") {
	resdir = builddir+"Electron.app/Contents/Resources/";
}

function editPlist(name) {
	let info = plist.parse(fs.readFileSync(builddir+name, 'utf8'));
	info.CFBundleDisplayName = "docoda";
	info.CFBundleIdentifier = "docoda";
	info.CFBundleName = "docoda";
	fs.writeFileSync(builddir+name, plist.build(info), 'utf8');
}

function renameExe() {
	if(platform=="win32") {
		fs.renameSync(builddir+"electron.exe", builddir+"docoda.exe");
	} else if(platform == "darwin") {
		editPlist("Electron.app/Contents/Info.plist");
		editPlist("Electron.app/Contents/Frameworks/Electron Helper.app/Contents/Info.plist");
		fs.renameSync(builddir+"Electron.app/Contents/MacOS/Electron", builddir+"Electron.app/Contents/MacOS/docoda");
		fs.renameSync(builddir+"Electron.app", builddir+"docoda.app");
		resdir = "build/"+platform+'-'+arch+"/docoda.app/Contents/Resources";
	} else {
		fs.renameSync(builddir+"electron", builddir+"docoda");
	}
}

function unpack(f) {
	console.log("using downloaded electron runtime");
	console.log("extracting electron");
	extract(zip, {dir: resolve(builddir)}, function (err) {
		if(err) {
			if(err.code == "EEXIST") {
			} else {
				console.log(err);
				return;
			}
		}
		f();
	});
}

function getFile(url, target) {
	var req = https.get(url, (res)=>{
		let size = 0;
		let totalSize = null;
		let haveData = false;
		if(res.statusCode=='302') {
			getFile(res.headers.location, target);
		} else if(res.statusCode=="200") {
			res.setEncoding("binary");
			if(res.headers['content-length']) {
				totalSize = parseInt(res.headers['content-length']);
			}

			var runtime = fs.createWriteStream(target+".part");
			res.on("data", (d)=>{
				size += d.length;
				if(totalSize) {
					process.stdout.write(Math.floor((size/totalSize)*100)+'%\r');
				} else {
					process.stdout.write(Math.floor(size/1024)+'k\r');
				}
				runtime.write(d, 'binary');
			});

			res.on('end', function() {
				if(size > 0) {
					fs.renameSync(target+".part", target);
					unpack(finish);
				}
			});
			req.end();
		}
	});

	req.on('error', (e)=>{ console.log("error ", e) });
}

function copyDir(source, target) {
	for(let f of fs.readdirSync(source, {encoding:"utf8"})) {
		if(isdir(source+'/'+f)) {
			mkdir(target+'/'+f);
			copyDir(source+'/'+f, target+'/'+f);
		} else {
			fs.copyFileSync(source+'/'+f, target+'/'+f);
		}
	}
}

function copyRuntime(f) {
	console.log("using local electron runtime");
	copyDir("node_modules/electron/dist/", builddir);
	f();
}

function finish() {
	renameExe();
	console.log("copying app bundle");
	fs.copyFileSync("app.asar", resdir+"/app.asar");
	console.log("done");
}

function download() {
	if(platform == os.platform() && arch == os.arch() && 0) {
		copyRuntime(finish);
	} else if(!fs.existsSync(zip)) {
		console.log("downloading electron runtime");
		getFile(url, zip);
	} else {
		unpack(finish);
	}
}

console.log("building docoda package");
asar.createPackage("package/", "app.asar", download);
