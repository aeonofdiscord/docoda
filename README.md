# docoda

docoda is a wikilike desktop tool for writing interlinked notes. It supports a very minimal formatting syntax.

## Building/running

Currently docoda requires [Node](https://nodejs.org) 8.11 and [Electron](https://electronjs.org) 2.0.8. There are no other dependencies.

If you have Node installed you should be able to run 
`npm install`
to install Electron and its dependencies, and then
`npm start`
to run docoda. You can also build a standalone docoda for your platform using 
`npm run build`

User data is saved in the .docoda folder under your home/user directory.

## License

docoda is licensed under the [ISC License](https://www.isc.org/downloads/software-support-policy/isc-license/), a non-restrictive software license compatible with the GNU GPL and the Open Source Definition. See LICENSE.txt for details.