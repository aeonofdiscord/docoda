const {app, BrowserWindow} = require("electron");

function createWindow() {
	let win = new BrowserWindow();
	win.maximize();
	if(!process.argv.includes("--debug"))
		win.setMenu(null);
	win.loadFile('index.html');
}

app.on("ready", createWindow);
app.on('window-all-closed', () => {
	app.quit();
});